# ta-ms-login

## Overview
The `ta-ms-project` microservice is designed for fetch data project purposes within the Technical Assessment system. It utilizes Java 17 and Spring Boot 3.2.0 to provide secure fetch projects functionality. 
The primary objective of this microservice is to connect the authentication in user authentication (ta-ms-login) so the project data could be fetched only by valid token, and its endpoint path is set to `/api/ms-project`.

notes:
```
Please run the ta-ms-login first (clone here: https://gitlab.com/hilman.trihatmojo/ta-ms-login) before run this ms
```

## Prerequisites
Ensure that the MySQL database is installed with the name `technical_assessment`. Additionally, build the project using the following Maven command:
```bash
mvn -U clean install
```
this microservices run in port `8086`
## Usages
After building the project, the application can be executed to handle project requests.
# Run the application
```
java -jar target/ta-ms-project-0.0.1-SNAPSHOT.jar
```
## Endpoints
1. Fetch Projects Endpoint

This endpoint is responsible for fetch project list data using filter from its code and status
```
@GetMapping
public ResponseEntity<ResponseGeneralDTO<List<ProjectDTO>>> getAllProjects(
            @ModelAttribute ProjectFilter filter) {
     return projectService.fetchAllProjects(filter);
 }
```
1. Fetch endpoint without filter 
* Endpoint: /api/ms-project/projects (http://localhost:8087/api/ms-project/projects)
* Method: GET
* Request Header:
```
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJmaXJzdF91c2VyIiwidXNlcm5hbWUiOiJmaXJzdF91c2VyIiwiZXhwIjoxNzAxNTMzNzI1fQ.X0-l8tNWWE2k0si7OH-elBmEEoifYb4WpeCQTilyfWo
```
* Response
```
{
    "code": "00",
    "description": "SUCCESS",
    "result": [
        {
            "id": 1,
            "code": "P001",
            "description": "Project 1",
            "createdBy": "first_user",
            "projectStatus": "STARTED"
        },
        {
            "id": 5,
            "code": "P005",
            "description": "Project 5",
            "createdBy": "first_user",
            "projectStatus": "STARTED"
        },
        {
            "id": 3,
            "code": "P003",
            "description": "Project 3",
            "createdBy": "first_user",
            "projectStatus": "DONE"
        },
        {
            "id": 4,
            "code": "P004",
            "description": "Project 4",
            "createdBy": "first_user",
            "projectStatus": "DONE"
        },
        {
            "id": 2,
            "code": "P002",
            "description": "Project 2",
            "createdBy": "first_user",
            "projectStatus": "DROP"
        }
    ]
}
```
2. Fetch endpoint with filter status
* Endpoint: /api/ms-project/projects (http://localhost:8087/api/ms-project/projects?status=DONE)
* Method: GET
* Request Header:
```
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJmaXJzdF91c2VyIiwidXNlcm5hbWUiOiJmaXJzdF91c2VyIiwiZXhwIjoxNzAxNTMzNzI1fQ.X0-l8tNWWE2k0si7OH-elBmEEoifYb4WpeCQTilyfWo
```
* Response
```
{
    "code": "00",
    "description": "SUCCESS",
    "result": [
        {
            "id": 3,
            "code": "P003",
            "description": "Project 3",
            "createdBy": "first_user",
            "projectStatus": "DONE"
        },
        {
            "id": 4,
            "code": "P004",
            "description": "Project 4",
            "createdBy": "first_user",
            "projectStatus": "DONE"
        }
    ]
}
```
3. Fetch endpoint with wrong header
* Endpoint: /api/ms-project/projects (http://localhost:8087/api/ms-project/projects)
* Method: GET
* Request Header:
```
Authorization: Bearer wrongtoken
```
* Response
```
{
    "error": "unauthorized.",
    "errorDescription": "Full authentication is required to access this resource"
}
```
Wrong token will lead to error message because every time endpoint hit it will also check token validity from service ta-ms-login

