package com.hilman.tamsproject.repository;

import com.hilman.tamsproject.model.Project;
import com.hilman.tamsproject.model.ProjectFilter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

    @Query(value = "SELECT p.*, s.name as status_name FROM Project p " +
            "JOIN Status s ON p.status = s.id " +
            "WHERE (:#{#filter.code} IS NULL OR p.code LIKE %:#{#filter.code}%) AND (:#{#filter.status} IS NULL OR s.name = :#{#filter.status})"
    , nativeQuery = true
    )
    List<Project> findProjectsByCodeAndStatus(@Param("filter") ProjectFilter filter);

}
