package com.hilman.tamsproject.controller;

import com.hilman.tamsproject.dto.ProjectDTO;
import com.hilman.tamsproject.dto.ResponseGeneralDTO;
import com.hilman.tamsproject.model.ProjectFilter;
import com.hilman.tamsproject.services.ProjectService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/projects")
public class ProjectController {
    final ProjectService projectService;

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping
    public ResponseEntity<ResponseGeneralDTO<List<ProjectDTO>>> getAllProjects(
            @ModelAttribute ProjectFilter filter) {
        return projectService.fetchAllProjects(filter);
    }
}
