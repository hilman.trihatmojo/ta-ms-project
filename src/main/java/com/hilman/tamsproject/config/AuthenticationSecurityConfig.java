package com.hilman.tamsproject.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.AuthorizeHttpRequestsConfigurer;
import org.springframework.security.web.util.matcher.RequestMatcher;


import java.util.List;

@Component
public class AuthenticationSecurityConfig {
    private final RestTemplate restTemplate;
    @Value("${security.authUrl}")
    private String authUri;
    private final ObjectMapper mapper;

    public AuthenticationSecurityConfig(RestTemplate restTemplate, ObjectMapper mapper) {
        this.restTemplate = restTemplate;
        this.mapper = mapper;
    }

    protected void configure(HttpSecurity httpSecurity, List<AntPathRequestMatcher> whiteListPatterns) throws Exception {
        httpSecurity.csrf(AbstractHttpConfigurer::disable).authorizeHttpRequests((auth) -> {
            whiteListPatterns.forEach((whiteListPattern) -> {
                ((AuthorizeHttpRequestsConfigurer.AuthorizedUrl)auth.requestMatchers(new RequestMatcher[]{whiteListPattern})).permitAll();
            });
            ((AuthorizeHttpRequestsConfigurer.AuthorizedUrl)auth.anyRequest()).authenticated();
        })
                .addFilterBefore(new BasicFilter(this.restTemplate, whiteListPatterns, this.authUri, mapper), BasicAuthenticationFilter.class).exceptionHandling((exception) -> {
            exception.authenticationEntryPoint((request, response, e) -> {
                System.out.println(e);
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                response.setContentType("application/json");
                response.getWriter().write("{ \"error\": \"unauthorized.\", \"errorDescription\" : \"Full authentication is required to access this resource\" }");
            });
        });
    }
}
