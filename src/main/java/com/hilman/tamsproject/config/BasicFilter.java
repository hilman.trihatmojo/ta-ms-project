
package com.hilman.tamsproject.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hilman.tamsproject.model.AuthenticatedUser;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.*;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.OncePerRequestFilter;

@EnableConfigurationProperties({SecurityProperties.class})
public class BasicFilter extends OncePerRequestFilter {
    private final RestTemplate restTemplate;
    private final List<AntPathRequestMatcher> whiteListPatterns;
    private final String userInfoUri;
    private final ObjectMapper mapper;

    protected boolean shouldNotFilter(HttpServletRequest request) {
        boolean shouldFilter = false;
        Iterator var3 = this.whiteListPatterns.iterator();

        while(var3.hasNext()) {
            AntPathRequestMatcher whiteListPattern = (AntPathRequestMatcher)var3.next();
            if (whiteListPattern.matches(request)) {
                shouldFilter = whiteListPattern.matches(request);
                break;
            }
        }

        return shouldFilter;
    }

    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        AuthenticatedUser authenticatedUser = this.getPrincipal(request);
        if (authenticatedUser != null) {
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(authenticatedUser, authenticatedUser.getCredentials(), new ArrayList<>());
            SecurityContext sc = SecurityContextHolder.getContext();
            sc.setAuthentication(authenticationToken);
        }

        filterChain.doFilter(request, response);
    }

//    @Override
//    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
//        Map<String, Object> errorDetails = new HashMap<>();
//
//        try {
//            AuthenticatedUser authenticatedUser = this.getPrincipal(request);
//            if (authenticatedUser != null) {
//                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(authenticatedUser, "", new ArrayList<>());
//                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
//            }
//
//        }catch (Exception e){
//            errorDetails.put("message", "Authentication Error");
//            errorDetails.put("details",e.getMessage());
//            response.setStatus(HttpStatus.FORBIDDEN.value());
//            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
//
//            mapper.writeValue(response.getWriter(), errorDetails);
//        }
//        filterChain.doFilter(request, response);
//    }

    private AuthenticatedUser getPrincipal(HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", request.getHeader("Authorization"));
        HttpEntity<String> entity = new HttpEntity((Object)null, headers);

        AuthenticatedUser principal;
        try {
            ResponseEntity<AuthenticatedUser> response = this.restTemplate.exchange(this.userInfoUri, HttpMethod.GET, entity, AuthenticatedUser.class, new Object[0]);
            principal = response.getBody();
        } catch (HttpClientErrorException var6) {
            this.logger.error(var6.getMessage());
            principal = null;
        }

        return principal;
    }

    public BasicFilter(final RestTemplate restTemplate, final List<AntPathRequestMatcher> whiteListPatterns, final String userInfoUri, ObjectMapper mapper) {
        this.restTemplate = restTemplate;
        this.whiteListPatterns = whiteListPatterns;
        this.userInfoUri = userInfoUri;
        this.mapper = mapper;
    }
}
