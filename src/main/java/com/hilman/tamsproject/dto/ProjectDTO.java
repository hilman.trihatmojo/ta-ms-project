package com.hilman.tamsproject.dto;

import com.hilman.tamsproject.model.Project;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDTO {
    private Long id;
    private String code;
    private String description;
    private String createdBy;
    private String projectStatus;

    public ProjectDTO(Project project){
        this.id = project.getId();
        this.code = project.getCode();
        this.createdBy = project.getCreatedBy();
        this.description = project.getDescription();
        this.projectStatus = project.getStatus().getName();
    }
}
