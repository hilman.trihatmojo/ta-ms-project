package com.hilman.tamsproject.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class RestService {
    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;
    private final Logger logger = LoggerFactory.getLogger(RestService.class);

    @Autowired
    public RestService(RestTemplate restTemplate, ObjectMapper objectMapper) {
        this.restTemplate = restTemplate;
        this.objectMapper = objectMapper;
    }

    private <T> T callGet(String url, HttpEntity<T> entity, Class<T> responseType, Map<String, Object> additionalProperties) {
        T responseObject;
        String requestHeader = "";
        String requestBody = "";
        String responseBody = "";

        try {
            logger.info("GET HTTP Request URL : {}", url);
            requestHeader = objectMapper.writeValueAsString(entity.getHeaders());
            logger.info("GET HTTP Request Header : {}", requestHeader);
            requestBody = objectMapper.writeValueAsString(entity.getBody());
            logger.info("GET HTTP Request Body : {}", requestBody);

            ResponseEntity<T> response = restTemplate.exchange(url, HttpMethod.GET, entity, responseType);
            logger.info("GET HTTP Response : {}", response);

            responseBody = objectMapper.writeValueAsString(response.getBody());
            logger.info("GET HTTP Response Body : {}", responseBody);

            responseObject = response.getBody();
        } catch (HttpClientErrorException | JsonProcessingException ex) {
            logger.error(ex.getMessage());
            responseObject = null;
        }

        return responseObject;
    }

    public <T> T get(String url, HttpEntity<T> entity, Class<T> responseType) {
        return this.callGet(url, entity, responseType, new HashMap<>());
    }
}

