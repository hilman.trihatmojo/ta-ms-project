package com.hilman.tamsproject.services;

import com.hilman.tamsproject.dto.ProjectDTO;
import com.hilman.tamsproject.dto.ResponseGeneralDTO;
import com.hilman.tamsproject.model.ProjectFilter;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ProjectService {
    ResponseEntity<ResponseGeneralDTO<List<ProjectDTO>>> fetchAllProjects(ProjectFilter filter);
}
