package com.hilman.tamsproject.services;

import com.hilman.tamsproject.dto.ProjectDTO;
import com.hilman.tamsproject.dto.ResponseGeneralDTO;
import com.hilman.tamsproject.enumeration.Status;
import com.hilman.tamsproject.model.Project;
import com.hilman.tamsproject.model.ProjectFilter;
import com.hilman.tamsproject.repository.ProjectRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectServiceImpl(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public ResponseEntity<ResponseGeneralDTO<List<ProjectDTO>>> fetchAllProjects(ProjectFilter filter) {
        try {
            List<Project> projects = projectRepository.findProjectsByCodeAndStatus(filter);
            if (projects.isEmpty()){
                ResponseGeneralDTO<List<ProjectDTO>> responseDTO = new ResponseGeneralDTO<>(Status.ERROR.getStatusCode(), Status.ERROR.getDescription(), null, "Project not found");
                return new ResponseEntity<>(responseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
            }
            List<ProjectDTO> projectDTOS = projects.stream()
                    .map(ProjectDTO::new)
                    .toList();
            ResponseGeneralDTO<List<ProjectDTO>> responseDTO = new ResponseGeneralDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), projectDTOS, null);
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
        }
        catch (Exception e){
            ResponseGeneralDTO<List<ProjectDTO>> responseDTO = new ResponseGeneralDTO<>(Status.ERROR.getStatusCode(), Status.ERROR.getDescription(), null, e.getMessage());
            return new ResponseEntity<>(responseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
