package com.hilman.tamsproject.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProjectFilter {
    private String code;
    private String status;
}
